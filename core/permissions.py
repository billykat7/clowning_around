from rest_framework                         import permissions


class AppointmentsPermissions(permissions.BasePermission):

    def has_permission(self, request, view, *args, **kwargs):

        user = request.user

        if request.user.is_authenticated:
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser

            if request.method == 'GET':
                if is_client or is_clown or is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'POST':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'PUT':
                if is_client or is_clown or is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'DELETE':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False
        else:
            return False


class AppointmentIsOwnerPermissions(permissions.BasePermission):
    """
        Object-level permission to only allow owners of an object to edit it.
        Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, appointment):

        current_user = request.user

        if request.user.is_authenticated:

            current_user_id  = current_user.id
            is_client        = current_user.is_client
            is_clown         = current_user.is_clown
            is_troupe_leader = current_user.is_troupe_leader
            is_superuser     = current_user.is_superuser

            if is_troupe_leader or is_superuser:
                return 'Y'

            if request.method != 'DELETE':

                if request.method == 'GET':
                    if view['request'] or view['issue']:
                        if is_clown and current_user_id == appointment.clown_id:
                            return 'CW'
                        return False

                if is_client and current_user_id == appointment.client_id:
                    return 'CL'

                if is_clown and current_user_id == appointment.clown_id:
                    return 'CW'

        return False


class ClientsPermissions(permissions.BasePermission):

    def has_permission(self, request, view, *args, **kwargs):

        user = request.user

        if request.user.is_authenticated:

            is_active        = user.is_active
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser

            if request.method == 'GET':
                if is_client or is_clown or is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            # elif request.method == 'POST':
            #     if is_troupe_leader or is_superuser:
            #         return True
            #     else:
            #         return False

            elif request.method == 'PUT':
                if is_clown or is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'DELETE':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False
        else:
            return False

    def has_object_permission(self, request, view, obj):

        user = request.user

        if request.user.is_authenticated:

            is_active        = user.is_active
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser


            user_id   = user.id
            obj_owner = obj.user_id

            if is_troupe_leader or is_superuser or user_id == obj_owner:
                print("user_id = ", user_id)
                print("obj = ", obj)
                print("obj_owner = ", obj_owner)
                return True

        return False


class ClownsPermissions(permissions.BasePermission):

    def has_permission(self, request, view, *args, **kwargs):

        user = request.user

        if request.user.is_authenticated:

            is_active        = user.is_active
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser

            if request.method == 'GET':
                if is_client or is_clown or is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'POST':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'PUT':
                if is_clown or is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'DELETE':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False
        else:
            return False

    def has_object_permission(self, request, view, obj):

        user = request.user

        if request.user.is_authenticated:

            is_active        = user.is_active
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser

            user_id   = user.id
            obj_owner = obj.user_id

            if is_troupe_leader or is_superuser or user_id == obj_owner:
                print("user_id = ", user_id)
                print("obj = ", obj)
                print("obj_owner = ", obj_owner)
                return True

        return False


class TroupeLeadersPermissions(permissions.BasePermission):

    def has_permission(self, request, view, *args, **kwargs):

        user = request.user

        if request.user.is_authenticated:

            is_active        = user.is_active
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser

            if request.method == 'GET':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'POST':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'PUT':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False

            elif request.method == 'DELETE':
                if is_troupe_leader or is_superuser:
                    return True
                else:
                    return False
        else:
            return False

    def has_object_permission(self, request, view, obj):

        user = request.user

        if request.user.is_authenticated:

            is_active        = user.is_active
            is_client        = user.is_client
            is_clown         = user.is_clown
            is_troupe_leader = user.is_troupe_leader
            is_superuser     = user.is_superuser

            user_id   = user.id
            obj_owner = obj.user_id

            if is_troupe_leader or is_superuser or user_id == obj_owner:
                print("user_id = ", user_id)
                print("obj = ", obj)
                print("obj_owner = ", obj_owner)
                return True

        return False
