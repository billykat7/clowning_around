from datetime               import datetime


class StringDateConverter:
    """
        Takes inputted Date string and return a python date object.
        If submitted date is not in accepted format, it will be nullified.
    """
    def __init__(self, in_date):
        super(StringDateConverter, self).__init__()
        self.in_date = in_date
        self.date    = None

        if '-' in self.in_date:
            if len(self.in_date) == 8:
                self.from_yy_mm_dd()

            if len(self.in_date) == 10:
                self.from_ccyy_mm_dd()

        elif '.' in self.in_date:
            if len(self.in_date) == 8:
                self.from_dotted_yy_mm_dd()

            if len(self.in_date) == 10:
                self.from_dotted_ccyy_mm_dd()

        elif '/' in self.in_date:
            if len(self.in_date) == 8:
                self.from_slashed_yy_mm_dd()

            if len(self.in_date) == 10:
                self.from_slashed_ccyy_mm_dd()

        print(self.date)

    def from_yy_mm_dd(self):
        self.date = datetime.strptime(f'{self.in_date}', '%y-%m-%d').date()

    def from_ccyy_mm_dd(self):
        self.date = datetime.strptime(f'{self.in_date}', '%Y-%m-%d').date()

    def from_dotted_yy_mm_dd(self):
        self.date = datetime.strptime(f'{self.in_date}', '%y.%m.%d').date()

    def from_dotted_ccyy_mm_dd(self):
        self.date = datetime.strptime(f'{self.in_date}', '%Y.%m.%d').date()

    def from_slashed_yy_mm_dd(self):
        self.date = datetime.strptime(f'{self.in_date}', '%y/%m/%d').date()

    def from_slashed_ccyy_mm_dd(self):
        self.date = datetime.strptime(f'{self.in_date}', '%Y/%m/%d').date()
