def request_get_input(i, self):

    user                  = self.request.user
    i['current_user']     = user
    i['current_user_id']  = user.id
    i['current_username'] = user.username
    i['is_staff']         = user.is_staff
    i['is_superuser']     = user.is_superuser
    i['is_client']        = user.is_client
    i['is_clown']         = user.is_clown
    i['is_troupe_leader'] = user.is_troupe_leader
    i['is_troupe_leader'] = user.is_troupe_leader

    return i
