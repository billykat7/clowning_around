from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AppointmentsConfig(AppConfig):
    name = "clowning_around.appointments"
    verbose_name = _("Appointments")

    def ready(self):
        try:
            import clowning_around.appointments.signals  # noqa F401
        except ImportError:
            pass
