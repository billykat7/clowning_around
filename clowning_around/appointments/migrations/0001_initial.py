# Generated by Django 2.2.4 on 2022-04-17 07:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0003_auto_20220414_0657'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime_created', models.DateTimeField(auto_now_add=True, verbose_name='DATE CREATED')),
                ('datetime_updated', models.DateTimeField(auto_now=True, verbose_name='DATE UPDATED')),
                ('last_updated_by', models.CharField(blank=True, max_length=50, null=True, verbose_name='LAST UPDATED BY')),
                ('bool_deleted', models.BooleanField(default=False, verbose_name='IS DELETED?')),
                ('name', models.CharField(blank=True, max_length=250, null=True, verbose_name='NAME')),
                ('date', models.DateField(blank=True, null=True, verbose_name='APPOINTMENT DATE')),
                ('status', models.CharField(choices=[('1', 'upcoming'), ('2', 'incipient'), ('3', 'completed'), ('4', 'cancelled')], default='1', max_length=1, verbose_name='STATUS')),
                ('rate', models.CharField(blank=True, max_length=25, null=True, verbose_name='RATE')),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='users.Client')),
                ('clown', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='users.Clown')),
            ],
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime_created', models.DateTimeField(auto_now_add=True, verbose_name='DATE CREATED')),
                ('datetime_updated', models.DateTimeField(auto_now=True, verbose_name='DATE UPDATED')),
                ('last_updated_by', models.CharField(blank=True, max_length=50, null=True, verbose_name='LAST UPDATED BY')),
                ('bool_deleted', models.BooleanField(default=False, verbose_name='IS DELETED?')),
                ('reason', models.TextField(blank=True, null=True, verbose_name='REASON')),
                ('appointment', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='appointments_requests', to='appointments.Appointment')),
            ],
        ),
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime_created', models.DateTimeField(auto_now_add=True, verbose_name='DATE CREATED')),
                ('datetime_updated', models.DateTimeField(auto_now=True, verbose_name='DATE UPDATED')),
                ('last_updated_by', models.CharField(blank=True, max_length=50, null=True, verbose_name='LAST UPDATED BY')),
                ('bool_deleted', models.BooleanField(default=False, verbose_name='IS DELETED?')),
                ('note', models.TextField(blank=True, null=True, verbose_name='NOTE')),
                ('appointment', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='appointments_issues', to='appointments.Appointment')),
            ],
        ),
    ]
