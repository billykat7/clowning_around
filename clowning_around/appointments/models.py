from django.db                      import models

from core.mixins                    import AuditFields


STATUS_CHOICES = [
    ('1', 'upcoming'),
    ('2', 'incipient'),
    ('3', 'completed'),
    ('4', 'cancelled'),
]

class Appointment(AuditFields):
    client = models.ForeignKey('users.Client', on_delete=models.CASCADE, null=True, blank=True)
    clown  = models.ForeignKey('users.Clown', on_delete=models.PROTECT, null=True, blank=True)
    name   = models.CharField('NAME', max_length=250, null=True, blank=True)
    date   = models.DateField('APPOINTMENT DATE', null=True, blank=True)
    status = models.CharField('STATUS', max_length=1, choices=STATUS_CHOICES, default='1')
    rate   = models.CharField('RATE', max_length=25, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'appointments'


class Request(AuditFields):
    appointment = models.ForeignKey('appointments.Appointment', on_delete=models.PROTECT, related_name='appointments_requests')
    reason      = models.TextField('REASON', null=True, blank=True)

    def __str__(self):
        return self.reason

    class Meta:
        app_label = 'appointments'


class Issue(AuditFields):
    appointment = models.ForeignKey('appointments.Appointment', on_delete=models.PROTECT, related_name='appointments_issues')
    note        = models.TextField('NOTE', null=True, blank=True)

    def __str__(self):
        return self.note

    class Meta:
        app_label = 'appointments'
