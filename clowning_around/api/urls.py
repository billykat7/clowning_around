from django.urls import path, include

from clowning_around.api.appointments.viewsets                  import AppointmentsAPIView
from clowning_around.api.appointments.viewsets                  import AppointmentsDetailAPIView
from clowning_around.api.clients .viewsets                      import ClientsAPIView
from clowning_around.api.clowns.viewsets                        import ClownsAPIView
from clowning_around.api.troupe_leaders.viewsets                import TroupeLeadersAPIView


app_name = 'api'

urlpatterns = [

    path('appointments/',           AppointmentsAPIView.as_view(),              name = 'appointments'),
    path('appointments/<id>',       AppointmentsDetailAPIView.as_view(),        name = 'appointments-detail'),
    path('clients/',                ClientsAPIView.as_view(),                   name = 'clients'),
    path('clowns/',                 ClownsAPIView.as_view(),                    name = 'clowns'),
    path('troupeleaders/',          TroupeLeadersAPIView.as_view(),             name = 'troupe-leaders'),

]
