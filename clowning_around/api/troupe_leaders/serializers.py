from rest_framework                                         import serializers

from clowning_around.users.models                           import TroupeLeader


# CLIENT REGISTER / CREATE SERIALIZER
class TroupeLeadersSerializer(serializers.ModelSerializer):

    class Meta:
        model   = TroupeLeader
        fields  = ('user', 'contact_name', 'contact_email', 'contact_number', 'last_updated_by')
