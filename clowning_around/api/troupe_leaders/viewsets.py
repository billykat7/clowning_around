from typing                                 import Union

from rest_framework                         import status
from rest_framework.authentication          import SessionAuthentication, BasicAuthentication
from rest_framework.response                import Response
from rest_framework.views                   import APIView

from core.permissions                       import TroupeLeadersPermissions


# RETRIEVE VIEW
class TroupeLeadersAPIView(APIView):

    authentication_classes  = (Union[SessionAuthentication, BasicAuthentication],)
    permission_classes      = (TroupeLeadersPermissions,)

    def get(self, request, **kwargs):

        print()

        return Response({"detail": ["No records found"]}, status=status.HTTP_200_OK)

    def post(self, request, **kwargs):

        print()

        return Response({"detail": ["You don't enough permissions."]}, status=status.HTTP_401_UNAUTHORIZED)

    def put(self, request, **kwargs):

        print()

        return Response({"detail": ["You don't enough permissions."]}, status=status.HTTP_401_UNAUTHORIZED)

    def delete(self, request, **kwargs):

        print()

        return Response({"detail": ["You don't enough permissions."]}, status=status.HTTP_401_UNAUTHORIZED)
