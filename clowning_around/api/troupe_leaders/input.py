def input_post_input(self):

    _i      = self.request.data
    client  = self.request.user

    i = {
        'client'         : client,
        'contact_name'   : _i.get('contact_name'  , None),
        'contact_email'  : _i.get('contact_email' , None),
        'contact_number' : _i.get('contact_number', None),

        # LOGIN - VARIABLES
        'username'       : _i.get('username', None),
        'password'       : _i.get('password', None),

        # REGISTRATION - VARIABLES
        'name'           : _i.get('name'        , None),
        'first_name'     : _i.get('first_name'  , None),
        'last_name'      : _i.get('last_name'   , None),
        'email'          : _i.get('email'       , None),
        'password1'      : _i.get('password1'   , None),
    }

    return i
