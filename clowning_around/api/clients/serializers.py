from rest_framework                                         import serializers

from clowning_around.users.models                           import Client


# CLIENT REGISTER / CREATE SERIALIZER
class ClientsSerializer(serializers.ModelSerializer):

    user_id = serializers.RelatedField(source='user', read_only=True)

    class Meta:
        model   = Client
        fields  = ('user_id', 'contact_name', 'contact_email', 'contact_number')
