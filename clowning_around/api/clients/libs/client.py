from django.db.models                               import Q

from clowning_around.users.models                   import Client


def get_client(i):
    clients = Client.objects.all()

    if i['contact_email']:
        client = clients \
            .filter(
                Q(contact_email = i['contact_email'])
            ) \
            .first()

    elif i['contact_number']:
        client = clients \
            .filter(
                Q(contact_number = i['contact_number'])
            ) \
            .first()
    else:
        client = clients.none()

    return client


def create_client(i, user):

    client = Client.objects.create(
        user           = user,
        contact_name   = i['contact_name'],
        contact_email  = i['contact_email'],
        contact_number = i['contact_number'],
    )

    return client
