from typing                                     import Union

from rest_framework                             import status
from rest_framework.authentication              import SessionAuthentication, BasicAuthentication
from rest_framework.permissions                 import AllowAny
from rest_framework.response                    import Response
from rest_framework.views                       import APIView

from core.permissions                           import ClientsPermissions
from .serializers                               import ClientsSerializer
from .input                                     import input_post_input
from .libs                                      import client as client_lib
from .libs                                      import user   as user_lib


# RETRIEVE VIEW
class ClientsAPIView(APIView):

    # authentication_classes  = (Union[SessionAuthentication, BasicAuthentication],)
    permission_classes      = (AllowAny,)

    def get(self, request, **kwargs):

        print()

        return Response({"detail": ["No records found"]}, status=status.HTTP_200_OK)

    def post(self, request, **kwargs):

        i = input_post_input(self)

        user = user_lib.get_user(i)

        if not user:
            return Response({"detail": ["Assigned User doesn't exists."]}, status=status.HTTP_204_NO_CONTENT)

        serializer = ClientsSerializer(data=request.data)

        if serializer.is_valid():

            client = client_lib.get_client(i)

            if client:
                return Response({"detail": ["Client already exists"]}, status=status.HTTP_409_CONFLICT)

            new_client = client_lib.create_client(i, user)

            if new_client:
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, **kwargs):

        print()

        return Response({"detail": ["You don't enough permissions."]}, status=status.HTTP_401_UNAUTHORIZED)

    def delete(self, request, **kwargs):

        print()

        return Response({"detail": ["You don't enough permissions."]}, status=status.HTTP_401_UNAUTHORIZED)
