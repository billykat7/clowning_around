def input_post_input(self):

    _i      = self.request.data
    client  = self.request.user

    i = {
        'client'         : client,

        # SENT INPUTS
        'user_id'        : _i.get('user_id'       , None),
        'contact_name'   : _i.get('contact_name'  , None),
        'contact_email'  : _i.get('contact_email' , None),
        'contact_number' : _i.get('contact_number', None),
    }

    return i
