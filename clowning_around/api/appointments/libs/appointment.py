from django.db.models                                       import Q
from django.utils                                           import timezone

from clowning_around.appointments.models                    import Appointment


def get_appointments_all():

    appointments = Appointment.objects.exclude(Q(bool_deleted = True)).select_related('client', 'clown')

    return appointments


def get_appointments_by_clown(i, clown):

    appointments = get_appointments_all().filter(Q(clown__user_id = clown.user_id) & Q(date = i['date']))

    return appointments


def get_appointments(i):

    appointments = get_appointments_all()

    if i['is_troupe_leader'] or i['is_superuser']:

        appointments = appointments

        if i['appointment_id']:
            appointments = appointments \
                .filter(
                    Q(id = i['appointment_id'])
                ) \
                .values(
                    'id', 'name', 'date', 'status', 'client_id', 'clown_id',

                    'client__user__is_troupe_leader', 'client__user__is_clown', 'client__user__is_client',
                    'client__user__name', 'clown__user__is_troupe_leader', 'clown__user__is_clown',
                    'clown__user__is_client', 'clown__user__name',
                )

        if i['client_id']:
            appointments = appointments \
                .filter(
                    Q(client_id = i['client_id'])
                ) \
                .values(
                    'id', 'name', 'date', 'status', 'client_id', 'clown_id',

                    'client__user__is_troupe_leader', 'client__user__is_clown', 'client__user__is_client',
                    'client__user__name', 'clown__user__is_troupe_leader', 'clown__user__is_clown',
                    'clown__user__is_client', 'clown__user__name',
                )

        if i['clown_id']:
            appointments = appointments \
                .filter(
                    Q(clown_id = i['clown_id'])
                ) \
                .values(
                    'id', 'name', 'date', 'status', 'client_id', 'clown_id',

                    'client__user__is_troupe_leader', 'client__user__is_clown', 'client__user__is_client',
                    'client__user__name', 'clown__user__is_troupe_leader', 'clown__user__is_clown',
                    'clown__user__is_client', 'clown__user__name',
                )

    elif i['is_client']:

        appointments = appointments.filter(Q(client_id = i['current_user_id']))

        if i['appointment_id']:
            appointments = appointments \
                .filter(
                    Q(id = i['appointment_id'])
                ) \
                .values(
                    'id', 'name', 'date', 'status', 'client_id', 'clown_id',

                    'client__user__is_troupe_leader', 'client__user__is_clown', 'client__user__is_client',
                    'client__user__name', 'clown__user__is_troupe_leader', 'clown__user__is_clown',
                    'clown__user__is_client', 'clown__user__name',
                )

        if i['client_id']:
            if str(i['client_id']).upper() != str(i['current_user_id']).upper():
                return [], 'NCU'

        if i['clown_id']:
            return [], 'NCU'

    elif i['is_clown']:

        appointments = appointments.filter(Q(clown_id = i['current_user_id']))

        if i['appointment_id']:
            appointments = appointments \
                .filter(
                    Q(id = i['appointment_id'])
                ) \
                .values(
                    'id', 'name', 'date', 'status', 'client_id', 'clown_id',

                    'client__user__is_troupe_leader', 'client__user__is_clown', 'client__user__is_client',
                    'client__user__name', 'clown__user__is_troupe_leader', 'clown__user__is_clown',
                    'clown__user__is_client', 'clown__user__name',
                )

        if i['client_id']:
            return [], 'NCU'

        if i['clown_id']:
            if str(i['clown_id']).upper() != str(i['current_user_id']).upper():
                return [], 'NCU'

    else:
        appointments = appointments.none()

    return appointments, 'Y'


def get_appointment(i):

    appointments = get_appointments_all()

    appointment  = appointments.filter(Q(id = i['pk'])).first()

    return appointment


def get_appointment_to_dict(i):

    appointment = {
        'client'           : i['appointment'].client.user.username,
        'clown'            : i['appointment'].clown.user.username,
        'name'             : i['appointment'].name,
        'date'             : i['appointment'].date,
        'status'           : i['appointment'].get_status_display(),
        'rate'             : i['appointment'].rate,
        'datetime_created' : i['appointment'].rate,
    }

    return appointment


def update_appointment(i):

    updated     = False
    accepted    = False
    appointment = {}

    updated_appointment = i['appointment']

    if i['permission'] == 'CL':
        accepted = True

        updated_appointment.rate             = i['rate'] if i['rate'] else updated_appointment.rate
        updated_appointment.last_updated_by  = i['current_username'] if i['current_username'] else updated_appointment.last_updated_by
        updated_appointment.datetime_updated = timezone.now()

    if i['permission'] == 'CW':
        accepted = True

        updated_appointment.status           = i['status'] if i['status'] else updated_appointment.status
        updated_appointment.last_updated_by  = i['current_username'] if i['current_username'] else updated_appointment.last_updated_by
        updated_appointment.datetime_updated = timezone.now()

    if i['permission'] == 'Y':
        accepted = True

        updated_appointment.client_id        = i['client']   if i['client']     else updated_appointment.client_id
        updated_appointment.clown_id         = i['clown']    if i['clown']      else updated_appointment.clown_id
        updated_appointment.name             = i['name']     if i['name']       else updated_appointment.name
        updated_appointment.date             = i['date']     if i['date']       else updated_appointment.date
        updated_appointment.status           = i['status']   if i['status']     else updated_appointment.status
        updated_appointment.rate             = i['rate']     if i['rate']       else updated_appointment.rate
        updated_appointment.last_updated_by  = i['current_username'] if i['current_username'] else updated_appointment.last_updated_by
        updated_appointment.datetime_updated = timezone.now()

    if accepted:
        updated = True
        updated_appointment.save()

        appointment = {
            'client' : updated_appointment.client.user.username,
            'clown'  : updated_appointment.clown.user.username,
            'name'   : updated_appointment.name,
            'date'   : updated_appointment.date,
            'status' : updated_appointment.get_status_display(),
            'rate'   : updated_appointment.rate,
        }

    return appointment, updated


def delete_appointment(i):

    deleted = False

    appointment = i['appointment']

    if i['permission'] == 'Y':
        appointment.bool_deleted     = True
        appointment.last_updated_by  = i['current_username'] if i['current_username'] else appointment.last_updated_by
        appointment.datetime_updated = timezone.now()
        appointment.save()

    return deleted


def create_appointment(i, client, clown):

    appointment, created = Appointment.objects.get_or_create(
        client           = client,
        clown            = clown,
        name             = i['name'],
        date             = i['date'],
        status           = i['status'],
        rate             = i['rate'],
        last_updated_by  = i['current_username']
    )

    appointment_dict = {
        'client' : appointment.client.user.username,
        'clown'  : appointment.clown.user.username,
        'name'   : appointment.name,
        'date'   : appointment.date,
        'status' : appointment.get_status_display(),
        'rate'   : appointment.rate,
    }

    return appointment_dict, created
