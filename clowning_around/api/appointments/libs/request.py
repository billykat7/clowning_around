from django.utils                                   import timezone

from clowning_around.appointments.models            import Request


def make_a_request(i):

    request, created = Request.objects.get_or_create(
        appointment_id   = i['appointment'].id,
        last_updated_by  = i['current_username'],
        defaults={
            'reason'           : i['reason'],
            'datetime_updated' : timezone.now(),
        }
    )

    return request
