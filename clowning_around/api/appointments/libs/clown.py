from django.db.models                               import Q

from clowning_around.users.models                   import Clown


def get_clown(i):

    clowns = Clown.objects.all()

    if i['clown_id']:
        clown = clowns.filter(Q(user_id = i['clown_id'])).first()

    else:
        clown = clowns.none()

    return clown
