from django.db.models                               import Q

from clowning_around.users.models                   import Client


def get_client(i):

    clients = Client.objects.all()

    if i['client_id']:
        client = clients.filter(Q(user_id = i['client_id'])).first()
    else:
        client = clients.none()

    return client


def get_client_requested(i):

    if str(i['request']) != str(i['appointment'].client_id):
        return None

    client = Client.objects\
        .filter(
            Q(user_id = i['appointment'].client_id)
        )\
        .values(
            'contact_name', 'contact_email', 'contact_number'
        )\
        .first()

    return client
