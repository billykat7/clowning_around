from django.utils                                   import timezone

from clowning_around.appointments.models            import Issue


def create_issue(i):

    issue, created = Issue.objects.get_or_create(
        appointment_id   = i['appointment'].id,
        last_updated_by  = i['current_username'],
        defaults={
            'note'             : i['issue'],
            'datetime_updated' : timezone.now(),
        }
    )

    issue_dict = {}

    if issue:
        issue_dict['appointment'] = issue.appointment.name
        issue_dict['note']        = issue.note
        issue_dict['logged_by']   = issue.last_updated_by

    return issue_dict
