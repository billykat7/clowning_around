from django.contrib.auth                            import get_user_model
from django.db.models                               import Q


User = get_user_model()


def get_user(i):
    users = User.objects.all()

    if i['user_id']:
        user = users \
            .filter(
                Q(id = i['user_id'])
            ) \
            .first()

    elif i['contact_email']:
        user = users \
            .filter(
                Q(email = i['contact_email'])
            ) \
            .first()
    else:
        user = users.none()

    return user
