from rest_framework                             import status
from rest_framework.authentication              import SessionAuthentication, BasicAuthentication
from rest_framework.response                    import Response
from rest_framework.views                       import APIView

from core.permissions                           import AppointmentsPermissions, AppointmentIsOwnerPermissions
from core.utils                                 import StringDateConverter
from clowning_around.api.clients.serializers    import ClientsSerializer
from .serializers                               import AppointmentsSerializer
from .input                                     import input_get_input, input_post_input
from .libs                                      import appointment  as appointment_lib
from .libs                                      import client       as client_lib
from .libs                                      import clown        as clown_lib
from .libs                                      import issue        as issue_lib
from .libs                                      import request      as request_lib


# LIST - POST - PUT - DELETE -  VIEW
class AppointmentsAPIView(APIView):

    authentication_classes  = [SessionAuthentication, BasicAuthentication,]
    permission_classes      = [AppointmentsPermissions,]

    def get(self, request, **kwargs):

        i = input_get_input(self)

        appointments, message = appointment_lib.get_appointments(i)

        serializer = AppointmentsSerializer(appointments, many=True)

        if appointments:
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response({"detail": "No record found"}, status=status.HTTP_204_NO_CONTENT)

        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, **kwargs):

        i = input_post_input(self)

        request.data['date'] = i['date'] = StringDateConverter(i['date']).date

        if not i['date']:
            return Response({"detail": "Date malformed."}, status=status.HTTP_400_BAD_REQUEST)

        serializer = AppointmentsSerializer(data=request.data)

        if serializer.is_valid():

            client = client_lib.get_client(i)

            clown = clown_lib.get_clown(i)

            if not client:
                return Response({"detail": "No client found for this appointment"}, status=status.HTTP_204_NO_CONTENT)

            if not clown:
                return Response({"detail": "No clown found to assign this appointment"}, status=status.HTTP_204_NO_CONTENT)

            appointment_by_clown = appointment_lib.get_appointments_by_clown(i, clown)

            if appointment_by_clown:
                return Response({"detail": f"Clown ({clown.user.username}) is already booked for this date."}, status=status.HTTP_409_CONFLICT)

            new_appointment, created = appointment_lib.create_appointment(i, client, clown)

            if new_appointment:
                serializer = AppointmentsSerializer(new_appointment)

            if created:
                return Response(serializer.instance, status=status.HTTP_201_CREATED)

            elif new_appointment:
                return Response(serializer.instance, status=status.HTTP_304_NOT_MODIFIED)

            return Response({"detail": "Appointment not created."}, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, **kwargs):

        print()

        return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)

    def delete(self, request, **kwargs):

        print()

        return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)


# RETRIEVE VIEW
class AppointmentsDetailAPIView(APIView):

    authentication_classes  = [SessionAuthentication, BasicAuthentication,]
    permission_classes      = [AppointmentsPermissions,]

    def get(self, request, **kwargs):

        i = input_get_input(self)

        if not i['pk'] or (i['pk'] and not (i['pk']).isdigit()):
            return Response({'detail': 'URL malformed.'}, status=status.HTTP_400_BAD_REQUEST)

        i['appointment']  = appointment_lib.get_appointment(i)

        if not i['appointment']:
            return Response({"detail": "No record found"}, status=status.HTTP_204_NO_CONTENT)

        i['permission'] = AppointmentIsOwnerPermissions.has_object_permission(self, request, i, i['appointment'])

        if not i['permission']:
            return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)

        appointment = appointment_lib.get_appointment_to_dict(i)

        if i['request']:
            if i['permission'] == 'CW':
                client = client_lib.get_client_requested(i)
                if not client:
                    return Response({"detail": "You do not have permission to request this client's details."}, status=status.HTTP_403_FORBIDDEN)

                request_lib.make_a_request(i)
                serializer_client = ClientsSerializer(client)
                return Response(serializer_client.instance, status=status.HTTP_200_OK)
            return Response({"detail": "You do not have permission to request this client's details."}, status=status.HTTP_403_FORBIDDEN)


        if i['issue']:
            if i['permission'] == 'CW':
                issue = issue_lib.create_issue(i)
                return Response(issue, status=status.HTTP_200_OK)
            return Response({"detail": "You do not have permission to request this client's details."}, status=status.HTTP_403_FORBIDDEN)

        serializer = AppointmentsSerializer(appointment)

        return Response(serializer.instance, status=status.HTTP_200_OK)

    def put(self, request, **kwargs):

        i = input_post_input(self)

        if not i['pk'] or (i['pk'] and not (i['pk']).isdigit()):
            return Response({'detail': 'URL malformed.'}, status=status.HTTP_400_BAD_REQUEST)

        i['appointment']  = appointment_lib.get_appointment(i)

        if not i['appointment']:
            return Response({"detail": "No record found"}, status=status.HTTP_204_NO_CONTENT)

        i['permission'] = AppointmentIsOwnerPermissions.has_object_permission(self, request, i, i['appointment'])

        if not i['permission']:
            return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)

        updated_appointment, updated = appointment_lib.update_appointment(i)

        if not updated:
            return Response({"detail": "Appointment not updated."}, status=status.HTTP_304_NOT_MODIFIED)

        serializer = AppointmentsSerializer(updated_appointment)

        return Response(serializer.instance, status=status.HTTP_200_OK)

    def delete(self, request, **kwargs):

        i = input_get_input(self)

        if not i['pk'] or (i['pk'] and not (i['pk']).isdigit()):
            return Response({'detail': 'URL malformed.'}, status=status.HTTP_400_BAD_REQUEST)

        i['appointment']  = appointment_lib.get_appointment(i)

        if not i['appointment']:
            return Response({"detail": "No record found"}, status=status.HTTP_204_NO_CONTENT)

        i['permission'] = AppointmentIsOwnerPermissions.has_object_permission(self, request, i, i['appointment'])

        if not i['permission']:
            return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)

        deleted = appointment_lib.delete_appointment(i)

        if not deleted:
            return Response({"detail": "Appointment not deleted."}, status=status.HTTP_304_NOT_MODIFIED)

        return Response({"detail": "Appointment successfully deleted."}, status=status.HTTP_200_OK)
