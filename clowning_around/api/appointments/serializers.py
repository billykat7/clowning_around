from rest_framework                                         import serializers

from clowning_around.appointments.models                    import Appointment


# CLIENT REGISTER / CREATE SERIALIZER
class AppointmentsSerializer(serializers.ModelSerializer):

    client_id = serializers.RelatedField(source='users.Client',    read_only=True)
    clown_id  = serializers.RelatedField(source='users.Clown',     read_only=True)
    status    = serializers.CharField(source='get_status_display', read_only=True)

    class Meta:
        model   = Appointment
        fields  = ['client_id', 'clown_id', 'name', 'date', 'status', 'rate']
