from common.input                           import request_get_input


def input_get_input(self):

    _i      = self.request.data
    _q      = self.request.GET
    _k      = self.kwargs
    client  = self.request.user

    i = {
        'client'         : client,

        # DETAIL INPUTS
        'pk'             : _k.get('id'),
        'request'        : _q.get('request'),
        'reason'         : _q.get('reason'),
        'issue'          : _q.get('issue'),

        # SENT INPUTS
        'appointment_id' : _i.get('appointment', None),
        'client_id'      : _i.get('client'     , None),
        'clown_id'       : _i.get('clown'      , None),
    }

    if not i['request']:
        i['request'] = _i.get('request')
    if not i['reason']:
        i['reason'] = _i.get('reason')
    if not i['issue']:
        i['issue'] = _i.get('issue')

    i = request_get_input(i, self)

    return i


def input_post_input(self):

    _i      = self.request.data
    _k      = self.kwargs
    client  = self.request.user

    i = {
        'client'    : client,

        # DETAIL INPUTS
        'pk'        : _k.get('id'),

        # SENT INPUTS
        'client_id' : _i.get('client', None),
        'clown_id'  : _i.get('clown' , None),
        'name'      : _i.get('name'  , None),
        'date'      : _i.get('date'  , None),
        'status'    : _i.get('status', None),
        'rate'      : _i.get('rate'  , None),
    }

    i = request_get_input(i, self)

    return i
