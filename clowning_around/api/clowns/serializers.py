from rest_framework                                         import serializers

from clowning_around.users.models                           import Clown


# CLIENT REGISTER / CREATE SERIALIZER
class ClownsSerializer(serializers.ModelSerializer):

    class Meta:
        model   = Clown
        fields  = ('user', 'contact_name', 'contact_email', 'contact_number', 'last_updated_by')
